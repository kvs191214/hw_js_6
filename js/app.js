console.log("Завдання 1 - знижка");

const product = {
    name: 'Phone',
    price: 10000,
    discount: '20%',
    fullPrice: function () {
        let value = '';
        for (let num of this.discount) {
            if (!isNaN(+num)) {
                value += num;
                value = +value;
            }
        }
        return this.price - this.price * value / 100
    }
}

console.log(product.fullPrice());

console.log("Завдання 2 - Привітання");

let user = {
    name: prompt("Ведіть ваше ім'я"),
    age: prompt('Скільки Вам років'),
    greeting: function () {
        return 'Привіт, мені ' + this.age + ' років'
    }
}

alert(user.greeting());

console.log("Завдання 3 - Клонування об'єкта");

const newUser = {
    name: 'Serhii',
    lastName: 'Kolesnikov',
    job: 'Front end',
    age: 30,
    hobby: ['swimming', 'travel', 'movies'],
    numbers: {
        num1: 856695858,
        num2: 48456847,
    },
    greeting: function () {
        return 'Привіт, мені ' + this.age + ' років'
    }
}

function clone (obj) {

    let objClone = {};
    for (let key in obj) {
        objClone[key] = obj[key];
    }
    return objClone
}

console.log(newUser);

let userClone = clone(newUser);

console.log(userClone);